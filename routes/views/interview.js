var express = require('express');
var router = express.Router();
var keystone = require('keystone');
var moment = require('moment');


var InterviewUser = keystone.list('InterviewUser');
var InterviewTester = keystone.list('InterviewTester');
var InterviewFeelSurvey = keystone.list('InterviewFeelSurvey');
var InterviewEventSurvey = keystone.list('InterviewEventSurvey');

router.post('/login', function(req, res, next) {
 



  var username = req.body.username;
  var password = req.body.password;

  var filter = {
    username: username
  };
 

  InterviewUser.model.findOne(filter).exec(function(err, Interview_User) {

    if (err) {
      console.log('Error occur when finding InterviewUser: ', err)
    }
    if ( Interview_User ) {     
     
     if(Interview_User.password===password)
     {
      console.log("success");
       res.json({

        Interview_User: Interview_User
       })

     }
     else
     {
      console.log("password_error");
       res.json({

       
       })

     }


   }
   else
   {console.log("username_error");
      res.json({

       
       })

   }
   
  });
});




router.post('/tester_info',  function(req, res, next) {


//var treatment=req.body.treatment;
//var room=req.body.room;


  var item = InterviewTester.model();
  var data={};

    data.testerId=req.body.testerId;

    data.testerName=req.body.testerName;
    data.testerBirthday=req.body.testerBirthday;
    data.testerGerder=req.body.testerGerder;
    data.testerGrade=req.body.testerGrade;
    data.testerDepartment=req.body.testerDepartment;
    data.testerSubject1=req.body.testerSubject1;
    data.testerSubject2=req.body.testerSubject2;
    data.testerLearned=req.body.testerLearned;
    data.testerMiddleSchool=req.body.testerMiddleSchool;
    data.testerHighSchool=req.body.testerHighSchool;
    data.testerLearnedCreditInternship=req.body.testerLearnedCreditInternship;
    data.testerLearnedCreditHalf_year=req.body.testerLearnedCreditHalf_year;
    data.testerLearnedCreditNot_yet=req.body.testerLearnedCreditNot_yet;


  console.log(data);

  item.getUpdateHandler(req).process(data, function(err){

  if(err) return res.json({error:err});

    console.log(item);


   res.json({
     Interview_Tester:item
   })

  });

  
});



router.post('/feel_survey',  function(req, res, next) {


//var treatment=req.body.treatment;
//var room=req.body.room;


  var item = InterviewFeelSurvey.model();
  var data={};


  data.question1=req.body.question1;
  data.question2=req.body.question2;
  data.question3=req.body.question3;
  data.question4=req.body.question4;
  data.question5=req.body.question5;
  data.question6=req.body.question6;
  data.question7=req.body.question7;
  data.question8=req.body.question8;
  data.question9=req.body.question9;
  data.question10=req.body.question10;
  data.question11=req.body.question11;
  


  console.log(data);

  item.getUpdateHandler(req).process(data, function(err){

  if(err) return res.json({error:err});

    console.log(item);


   res.json({
     Interview_Feel_Survey:item
   })

  });

  
});




router.post('/event_survey',  function(req, res, next) {


//var treatment=req.body.treatment;
//var room=req.body.room;


  var item = InterviewEventSurvey.model();
  var data={};


  data.question1=req.body.question1;
  data.question2=req.body.question2;
  data.question3=req.body.question3;

  


  console.log(data);

  item.getUpdateHandler(req).process(data, function(err){

  if(err) return res.json({error:err});

    console.log(item);


   res.json({
     Interview_Event_Survey:item
   })

  });

  
});







module.exports = router;