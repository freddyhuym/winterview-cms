var keystone = require('keystone');
var Types = keystone.Field.Types;


/**
 * CarebeautyUser Model
 * ==========
 */
var InterviewUser = new keystone.List('InterviewUser', {
//hidden: modelsConfig.indexOf('CarebeautyUser') === -1,
});

InterviewUser.add({

username:{type: String, initial: true},
password: { type: String, initial: true },
realname: { type: String, initial: true }

});




InterviewUser.defaultColumns = 'username, realname';
InterviewUser.register();