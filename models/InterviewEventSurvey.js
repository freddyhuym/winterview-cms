var keystone = require('keystone');
var Types = keystone.Field.Types;
var moment = require('moment');


var InterviewEventSurvey = new keystone.List('InterviewEventSurvey', {
	
});

InterviewEventSurvey.add({
	question1: { type: String},
    question2: { type: String},
	question3: { type: String}

});


InterviewEventSurvey.schema.post('init', function() {
    var m = moment(this.time);
    this.time = m.format('YYYY-MM-DD HH:mm:ss.SSS');
    this.ms = m.valueOf();
});


InterviewEventSurvey.defaultColumns = 'question1, question2, question3';
InterviewEventSurvey.register();