var keystone = require('keystone');
var Types = keystone.Field.Types;
var moment = require('moment');


var InterviewTester = new keystone.List('InterviewTester', {
	
});

InterviewTester.add({

    testerId: { type: String, label: '受試者編號'},
	testerName: { type: String, label: '姓名'},
	testerBirthday:{type: Date, label: '生日'},
    testerGerder: {type: String, label: '性別'},
    testerGrade:{type: String, label: '年級'},
    testerDepartment:{type: String, label: '學系'},
    testerMiddleSchool: { type: String , label: '是否預計在國中任教'},
    testerHighSchool: { type: String , label: '是否預計在高中任教'},
    testerSubject1:{type: String, label: '預計任教之科目1'},
    testerSubject2:{type: String, label: '預計任教之科目2'},
    testerLearned:{type: String, label: ' 是否已經修習班級經營師培課程'},

    testerLearnedCreditInternship: { type: String , label: '有，已修習分科/分領域（群科）教學實習'},
    testerLearnedCreditHalf_year: { type: String , label: '有，已修習半年實地實習'},
    testerLearnedCreditNot_yet: { type: String , label: '無'}
     
});


InterviewTester.schema.post('init', function() {
    var m = moment(this.time);
    this.time = m.format('YYYY-MM-DD HH:mm:ss.SSS');
    this.ms = m.valueOf();
});


InterviewTester.defaultColumns = 'testerName, testerGerder, testerAge, TesterTeachingYear, TesterTeacherYear';
InterviewTester.register();