var keystone = require('keystone');
var Types = keystone.Field.Types;
var moment = require('moment');


var InterviewFeelSurvey = new keystone.List('InterviewFeelSurvey', {
	
});

InterviewFeelSurvey.add({
	question1: { type: String},
    question2: { type: String},
	question3: { type: String},
    question4: { type: String},
    question5: { type: String},
    question6: { type: String},
    question7: { type: String},
    question8: { type: String},
    question9: { type: String},
    question10: { type: String},
    question11: { type: String},
});


InterviewFeelSurvey.schema.post('init', function() {
    var m = moment(this.time);
    this.time = m.format('YYYY-MM-DD HH:mm:ss.SSS');
    this.ms = m.valueOf();
});


InterviewFeelSurvey.defaultColumns = 'question1, question2, question3, question4, question5';
InterviewFeelSurvey.register();